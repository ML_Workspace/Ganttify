<?php
	//Database connection information, default table name is 'ganttify'
	include 'lib/db.php';	//Remove this line
	//$db = new mysqli('database location', 'username', 'password', 'database name');

	include 'lib/http.php';
	include 'lib/mysqli.php';
	include 'lib/safe.php';
	include 'lib/functions.php';
	if ($db->connect_errno) {
		http(500);
		die($db->connect_errno . ' ' . $db->connect_error);
		exit;
	}
	if (!$db->set_charset('utf8mb4')) mysqli_echo_error();
	parse_str(file_get_contents("php://input"),$_HTTP);
	switch ($_SERVER['REQUEST_METHOD']) {
		case 'GET':
			if ($result = $db->query("SELECT id,name,cont,start,end,depen,type FROM ganttify;")) {
				unset($list);
				while ($i = $result->fetch_assoc()) $list[] = $i;
				csDepen();
				if (!isset($_GET['start'])) $_GET['start'] = 0;
				if (!isset($_GET['end'])) $_GET['end'] = PHP_INT_MAX;
				time_filter();
				if (isset($_GET['depen'])) depen_filter();
				echo json_encode(array_values($list));
				sqlExit();
			}
			else mysqli_echo_error();
			break;
		case 'POST':
			checkTime(safe($_HTTP['start']), safe($_HTTP['end']));
			if ($db->query("INSERT INTO ganttify (name, cont, start, end, depen, type) VALUES (\"" . safe($_HTTP['name']) . "\", \"" . safe($_HTTP['cont']) . "\", " . safe($_HTTP['start']) . ", " . safe($_HTTP['end']) . ", \"" . safe($_HTTP['depen']) . "\", \"" . safe($_HTTP['type']) . "\");")) {
				http(201);
				sqlExit();
			}
			else mysqli_echo_error();
			break;
		case 'PUT':
			if (isset($_HTTP['id'])) {
				if (isset($_HTTP['name'])) {
					if ($db->query("UPDATE ganttify SET name=\"" . safe($_HTTP['name']) . "\" WHERE id = " . safe($_HTTP['id']) . ";")) http(205);
					else mysqli_echo_error();
				}
				if (isset($_HTTP['cont'])) {
					if ($db->query("UPDATE ganttify SET cont=\"" . safe($_HTTP['cont']) . "\" WHERE id = " . safe($_HTTP['id']) . ";")) http(205);
					else mysqli_echo_error();
				}
				if (isset($_HTTP['depen'])) {
					if (($db->query("SELECT COUNT(*) AS de FROM ganttify WHERE id = " . safe($_HTTP['depen']) . ";")->fetch_assoc())['de']) {
						if (safe($_HTTP['depen']) != safe($_HTTP['id'])) {
							if ($db->query("UPDATE ganttify SET depen=\"" . safe($_HTTP['depen']) . "\" WHERE id = " . safe($_HTTP['id']) . ";")) http(205);
							else mysqli_echo_error();
						}
						else {
							http(400);
							die('Self-dependent is not allowed.');
							sqlExit();
						}
					}
					else {
						http(400);
						die('Dependent id does not exist.');
						sqlExit();
					}
				}
				if (isset($_HTTP['type'])) {
					if ($db->query("UPDATE ganttify SET type=\"" . safe($_HTTP['type']) . "\" WHERE id = " . safe($_HTTP['id']) . ";")) http(205);
					else mysqli_echo_error();
				}
				if (isset($_HTTP['start']) || isset($_HTTP['end'])) {
					unset($list);
					if ($list = ($db->query("SELECT start,end FROM ganttify WHERE id = " . safe($_HTTP['id']) . ";")->fetch_assoc())) {
						$start = isset($_HTTP['start']) ? safe($_HTTP['start']) : $list['start'];
						$end = isset($_HTTP['end']) ? safe($_HTTP['end']) : $list['end'];
					}
					else mysqli_echo_error();
					checkTime($start, $end);
					if ($db->query("UPDATE ganttify SET start=" . $start . ", end=" . $end . " WHERE id = " . safe($_HTTP['id']) . ";")) http(205);
					else mysqli_echo_error();
				}
				sqlExit();
			}
			else {
				http(400);
				die('There was a missing or invalid parameter.');
				sqlExit();
			}
			break;
		case 'DELETE':
			if (isset($_HTTP['id'])) {
				unset($list);
				$list[] = $_HTTP['id'];
				comDel($_HTTP['id']);
				sqlExit();
			}
			else {
				http(400);
				die('There was a missing or invalid parameter.');
				sqlExit();
			}
			break;
		default:
			http(400);
			die('Method error.');
			sqlExit();
			break;
	}
